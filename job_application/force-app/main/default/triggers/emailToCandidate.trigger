trigger emailToCandidate on Candidate_Ashutosh__c (before update,after update) {
    //asyncronus apex class send mail to candidate when his status is hired
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
              List<ID> lstid = new List<ID>();
             for(Candidate_Ashutosh__c a : Trigger.new){
                 //system.debug('id1-'+lstid);
                 //system.debug('trigger.newwwwawwwerwer');
                 //system.debug(Trigger.new);
                 if(a.Status__c == 'Hired.'){
                   //  system.debug(a.id);
                    lstid.add(a.Id); 
                 }
             }
            //system.debug('id'+lstid);
            //system.debug('a.id');
            
              SendMailToCandidate.sendMail(lstid);
        }
    }
}