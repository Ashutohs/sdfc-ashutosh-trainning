@isTest
public class DeActivateJobsTest {
   @isTest 
    public static void testExpireJob(){
       List<Job_Ashutosh__c> jobdata = new List<Job_Ashutosh__c>();
       Contact con = new Contact(LastName = 'ramtest');
       insert con;
       List<Contact> contactId = [SELECT id FROM Contact WHERE LastName=:'ramtest' LIMIT 1];
       for(Integer i=0; i < 10; i++){
           //name job admin1
           jobdata.add(new Job_Ashutosh__c(Active__c=true,No_Of_Position__c=i, Expires_On__c=System.today().addDays(-5), Name = 'a035g000001VTEX', Salary_Offered__c =1000+i, Manager__c = contactId[0].id));
       }
       insert jobdata;
       
       Map<Id, Job_Ashutosh__c> jobmap = new Map<Id, Job_Ashutosh__c>(jobdata);
       List<Id> jobIds = new List<Id>(jobmap.keySet());
        
       Test.startTest();
       // Schedule the test job
        String CRON_EXP = '0 0 0 3 9 ? 2022';
      // String jobId = System.schedule('testScheduledApex',DeActivateJobsTest.CRON_EXP, new DeActivateJobsTest());
       List<Task> lt = [SELECT Id FROM Task WHERE WhatId IN :jobIds];  
     
       
       // Verify scheduled job has not run yet.
       System.assertEquals(0, lt.size(), 'Tasks exist before job has run');
       Test.stopTest();
       
       //Now the scheduled job has executed
       //check our tasks created or not
       List<Task> tasklt = [SELECT Id FROM Task WHERE WhatId IN :jobIds];
       //System.assertEquals(jobIds.size(), tasklt.size(),'Tasks were not created');
       System.assertEquals(200, jobdata.size());
   }
}