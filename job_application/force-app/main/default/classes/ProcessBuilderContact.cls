public class ProcessBuilderContact {
    @InvocableMethod
    public static void autoUpdateContactFields(List<ID> contactId){
                System.debug('contact id here !!'+contactId);

        Contact contactVar = [SELECT id,account.name,Email,MailingAddress,LastName FROM Contact where LastName LIKE '%test%' AND id =: contactId];
        System.debug('contact select records'+contactVar.id);
        Contact con = [SELECT id,Email FROM Contact Where id =: contactVar.id];
        System.debug('contact con var'+con);
        String name = contactVar.account.name;
        System.debug('account name'+name);
        Account account = [SELECT Name,BillingStreet,BillingCity,BillingPostalCode,BillingState,BillingCountry FROM Account where Name =:name];
        //assign account values to contact
        contactvar.MailingStreet = account.BillingStreet;
        contactvar.MailingCity = account.BillingCity;
        contactvar.MailingState = account.BillingState;
        contactvar.MailingPostalCode = account.BillingPostalCode;
        contactvar.MailingCountry = account.BillingCountry;
        update contactvar;
        
        //send mail to contact 
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(con.Email);
        mail.setToAddresses(sendTo);
        mail.setSubject('Auto Populate Address');
        String body = 'while creating contact the address of account get auto populated';
        mail.setHtmlBody(body);
        mails.add(mail);
        Messaging.sendEmail(mails);   
    }
}