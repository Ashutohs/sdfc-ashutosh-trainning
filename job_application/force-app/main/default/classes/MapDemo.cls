public with sharing class MapDemo {
    public void  mapDemo() {
        Map<String, Integer> mapName = new Map<String, Integer>();
        mapName.put('first',1);
        mapName.put('second',2);
        mapName.put('third',3);
        System.debug('mapvalues'+mapName);
        mapName.put('third',300);
        System.debug('overrides'+mapName);
        List<String>listStr = new List<String>();
        listStr.add('ashutosh');
        listStr.add('ram');
        for(String a:listStr){
            System.debug('  list values    '+a);
        }
		//List<Contact> lstcon = [SELECT id FROM Contact];
		List<ID> lstcon = new List<ID>();
        lstcon.add('0035g000002FH15AAG');
        System.debug('list of contact'+'==='+lstcon);
    }
}