global class DeActivateJobs implements schedulable{
    global void execute(SchedulableContext ctx){
        List<Job_Ashutosh__c> jlst = new List<Job_Ashutosh__c>();
        SYSTEM.debug('LIST OF EXPIRE DATE EXCEEDED JOBS ');
        SYSTEM.debug(jlst);
        for(Job_Ashutosh__c jobobj :  [SELECT id,Expires_On__c,Active__c FROM Job_Ashutosh__c WHERE Expires_On__c < Today]){
            jobobj.Active__c = false;
            jlst.add(jobobj);
        }
        update(jlst);
    }
   // DeActivateJobs dejobobj = new DeActivateJobs();
   // String sch = '20  30 8 10 2  ?';
   // String jobId = system.schedule('Remind Opp Owners', sch, dejobobj);
    
}