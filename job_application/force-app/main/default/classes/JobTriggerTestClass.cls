@isTest
public class JobTriggerTestClass {
    @testSetup
    public static void setupMethod(){
        Contact con = new Contact(LastName = 'Hari Haree');
        insert con;
        List<Contact> contactId = [SELECT id FROM Contact WHERE LastName=:'Hari Haree' LIMIT 1];
        List<Job_Ashutosh__c> joblist = new  List<Job_Ashutosh__c>();
        List<Candidate_Ashutosh__c> lstCandidate = new  List<Candidate_Ashutosh__c>();
        for(Integer i = 0 ; i<10 ; i++) {
            jobList.add(new Job_Ashutosh__c(Active__c=true,No_Of_Position__c=i, Name = 'a035g0000017V6A', Salary_Offered__c =1000+i, Manager__c	 = contactId[0].id));
            lstCandidate.add(new Candidate_Ashutosh__c(First_Name__c='Ram'+i,Last_Name__c='Pawar'+i,Email__c='Ram'+i+'@gmail.com',Application_Date__c = null, Expected_Salary__c = 0,Name__c = 'a035g000001VNt9', Country__c = 'India',State__c = 'Maharashtra', Status__c = 'Hired.'));
        }
        insert lstCandidate;
        insert joblist; 
    }
    //test method 1
    @isTest
    public static void checkSalary(){
        Test.startTest();
        try{
            List<Candidate_Ashutosh__c> lstCandidate = [SELECT id,First_Name__c,Last_Name__c,Email__c,status__c,Name__c,Expected_Salary__c FROM Candidate_Ashutosh__c WHERE Expected_Salary__c=0];
            Candidate_Ashutosh__c cdata = new Candidate_Ashutosh__c(Expected_Salary__c = null);
            cdata.Expected_Salary__c = null;
            
            update cdata;
            if(cdata.Expected_Salary__c == null){
                //   Candidate__c cdata = new Candidate__c(Expected_Salary__c = null);
                Database.SaveResult result = Database.insert(cdata, false);
                System.assertEquals('Please fill salary field',result.getErrors()[0].getMessage());
            }
        }catch(Exception e){
            String message = e.getMessage();
            //System.assert(message.contains('expected salary is reqeuuired'));
        }
        Test.stopTest();
    }
    //test method 2
   /* @isTest
    public static void deleteJob(){
        Test.startTest();
        try{
            List<Job_Ashutosh__c>jobList = [SELECT Active__c FROM Job_Ashutosh__c WHERE Active__c = true];
            Delete jobList;
        }catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('job is active cant be deleted'));
        }
        Test.stopTest();
    }*/
    //test method 2
    @isTest 
    public static void activeStatusJob(){
       Job_Ashutosh__c nonActiveJob = [SELECT Active__c FROM Job_Ashutosh__c WHERE Active__c=:true LIMIT 1];
       Candidate_Ashutosh__c canddata = [SELECT Name__c FROM Candidate_Ashutosh__c WHERE Job__c != NULL LIMIT 1];
       Test.startTest();      
       try{
           nonActiveJob.Active__c = false;
           update nonActiveJob;
           	if(nonActiveJob.get(canddata.Name__c) != null){              
               Job_Ashutosh__c cdata = new Job_Ashutosh__c(Active__c = false);
               Database.SaveResult result = Database.insert(cdata, false);
               System.assertEquals('This Job is not active. You can not apply for this job',result.getErrors()[0].getMessage());
           }
           
       }catch(DmlException e){
           String message = e.getMessage();
           system.assertEquals('This Job is not active. You can not apply for this job', message);
       }
       Test.stopTest();
   }
    
}