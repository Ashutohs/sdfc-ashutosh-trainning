public class SendMailToCandidate {
    
    @Future(callout=true)
    public static void sendMail(List<ID> listid){
        
        //ID Id = ApexPages.currentPage().getParameters().get('id');
        List<Candidate_Ashutosh__c> crecord = [SELECT id,Email__c FROM Candidate_Ashutosh__c WHERE id IN: listid];
        //system.debug('candidate record');
        //system.debug(crecord);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Attachment attach = new Attachment();
        for(Candidate_Ashutosh__c candidate : crecord) {
            system.debug(candidate);
            //getting data from VF page
            PageReference pdf = Page.CandidateDetailPDF;
            pdf.getParameters().put('id',candidate.id);
            pdf.setRedirect(true);
            
            Blob b = pdf.getContentAsPDF();
            
            //creating the File Attachment for Mail
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            attach.Body = b;
            efa.setFileName('CandidateDetails.pdf');
            efa.setBody(b);
            attach.Name =efa.filename;
            attach.IsPrivate = false;
            attach.ParentId = candidate.Id;
          insert attach;
            fileAttachments.add(efa);
            
            //Creating email structure
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            List<String> sendTo = new List<String>();
            sendTo.add(candidate.Email__c);
            system.debug('candidate mail');
            system.debug(candidate.Email__c);
            mail.setToAddresses(sendTo);
            mail.setSubject('Job Offer');  
            mail.setPlainTextBody( 'Congrats You are Hired' );
            mail.setFileAttachments(fileAttachments);
            mails.add(mail);
            
        }
        system.debug('mail sent');
        Messaging.sendEmail(mails);    //sending mails
        system.debug('mail sent 1');
    }
}